FROM php:7.3-apache
RUN docker-php-ext-install pdo pdo_mysql
COPY public/ /var/www/html/
COPY ca-certificate.crt /etc/database/
