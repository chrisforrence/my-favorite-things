<?php

$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    PDO::ATTR_EMULATE_PREPARES   => false,
    PDO::MYSQL_ATTR_SSL_CA       => '/etc/database/ca-certificate.crt',
];
$dsn = 'mysql:host=' . $_ENV['DATABASE_HOST']
    . ';port=' . $_ENV['DATABASE_PORT']
    . ';dbname=' . $_ENV['DATABASE_NAME']
    . ';charset=utf8mb4';

try {
     $pdo = new PDO($dsn, $_ENV['DATABASE_USERNAME'], $_ENV['DATABASE_PASSWORD'], $options);
} catch (PDOException $e) {
     throw new PDOException($e->getMessage(), (int)$e->getCode());
}

$stmt = $pdo->query('SELECT * FROM things WHERE is_favorite = 1');
$rows = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Favorite Things</title>
</head>
<body>
    <p>Here are my favorite things:</p>
    <ul>
        <?php foreach($rows as $row): ?>
        <li><?= htmlspecialchars($row->name) ?></li>
        <?php endforeach ?>
    </ul>
</body>
</html>
